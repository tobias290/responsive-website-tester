import React from "react";
import PropTypes from "prop-types";

import "../css/landing-page.css";

export default class LandingPage extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            urlInput: "",
        };

        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    /**
     * Updates the URL input.
     *
     * @param event
     */
    handleChange(event) {
        this.setState({urlInput: event.target.value});
    }

    /**
     * Loads the main page.
     */
    onSubmit() {
        this.props.onSubmit(this.state.urlInput);
    }

    /**
     * Render landing page.
     */
    render() {
        return (
            <div className="landing-page-center">
                <h1 className="main-title">Responsive Website Tester</h1>

                <div className="landing-page-center">
                    <form className="landing-form" onSubmit={this.onSubmit}>
                        <input className="landing-form__input" name="url" type="text" placeholder="Enter URL..." value={this.state.urlInput} onChange={this.handleChange} />
                        <input className="landing-form__submit" type="submit" value="Load URL" />
                    </form>
                </div>
            </div>
        );
    }
}

LandingPage.propTypes = {
    onSubmit: PropTypes.func,
};
