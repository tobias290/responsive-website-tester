import React from "react";
import PropTypes from "prop-types";

import "../css/slides.css";

export default class Slides extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            index: 0,
        }
    }

    /**
     * Sets which slide to show.
     *
     * @param {number} index - Index of the slide to show.
     */
    setIndex(index) {
        this.setState({
            index: index,
        });

        this.props.onSlideChange(index);
    }

    /**
     * Render slides.
     */
    render() {
        return (
            <div className="slides">
                <div className="slides__frame">
                    <button
                        className="option-button option-button--large-font"
                        onClick={() => this.setIndex(
                            this.state.index - 1 < 0 ?
                            this.props.children.length - 1 :
                            this.state.index - 1
                        )}
                        title={
                            this.state.index - 1 < 0 ?
                            this.props.children[this.props.children.length - 1].props.name :
                            this.props.children[this.state.index - 1].props.name
                        }
                       >
                        <i className="fas fa-chevron-left" />
                    </button>

                    {this.props.children[this.state.index]}

                    <button
                        className="option-button option-button--large-font"
                        onClick={() => this.setIndex(
                            this.state.index + 1 >= this.props.children.length ?
                            0 :
                            this.state.index + 1
                        )}
                        title={
                            this.state.index + 1 >= this.props.children.length ?
                            this.props.children[0].props.name :
                            this.props.children[ this.state.index + 1].props.name
                        }
                    >
                        <i className="fas fa-chevron-right" />
                    </button>
                </div>
                <div className="slides__bullets">
                    {this.props.children.map((iframe, i) =>
                        <div
                            key={i}
                            className={
                                this.state.index === i ?
                                "slides__bullets__bullet slides__bullets__bullet--active" :
                                "slides__bullets__bullet"
                            }
                            title={`${iframe.props.name} (${iframe.props.width}x${iframe.props.height})`}
                            onClick={() => this.setIndex(i)}
                        />
                    )}
                </div>
            </div>
        );
    }
}

Slides.propTypes = {
    onSlideChange: PropTypes.func,
};

Slides.defaultProps = {
    onSlideChange: (index) => {},
};
