import React from "react";

import "../css/app.css";
import IFrame from "./IFrame";
import LandingPage from "./LandingPage";
import OptionsBar from "./OptionsBar";
import Slides from "./Slides";
import update from "immutability-helper";

import iFrameData from "../iframeData";

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            showLandingPage: true,
            currentURL: "",
            iFrameData: iFrameData,
            currentIframeIndex: 0,
            showSideBySide: false,
        };
    }

    /**
     * Pull data from session storage on page load.
     */
    componentDidMount() {
        this.setState({
            ...JSON.parse(sessionStorage.getItem("state"))
        });
    }

    /**
     * Save data is session storage when state is updated.
     */
    componentDidUpdate(prevProps, prevState, snapshot) {
        sessionStorage.setItem("state", JSON.stringify(this.state));
    }

    /**
     * Render the page.
     */
    render() {
        return this.state.showLandingPage ? this.renderLandingPage() : this.renderMainPage();
    }

    /**
     * Render landing page.
     */
    renderLandingPage() {
        return <LandingPage
            onSubmit={(url) => this.setState({
                showLandingPage: false,
                currentURL: url,
            })}
        />;
    }

    /**
     * Render the main (iFrames) page.
     */
    renderMainPage() {
        return (
            <>
                <OptionsBar
                    screenType={this.state.iFrameData[this.state.currentIframeIndex].name}
                    screenHeight={this.state.iFrameData[this.state.currentIframeIndex].height}
                    screenWidth={this.state.iFrameData[this.state.currentIframeIndex].width}
                    zoom={this.state.iFrameData[this.state.currentIframeIndex].zoom}
                    onURLChange={(url) => {
                        this.setState({
                            currentURL: url
                        })
                    }}
                    onZoomChange={(zoom) => {
                        this.setState({
                            iFrameData: update(this.state.iFrameData, {
                                [this.state.currentIframeIndex]: {zoom : {$set: zoom}}
                            })
                        })
                    }}
                    onRotate={() => {
                        this.setState({
                            iFrameData: update(this.state.iFrameData, {
                                [this.state.currentIframeIndex]: {
                                    mainOrientation : {
                                        $set: !this.state.iFrameData[this.state.currentIframeIndex].mainOrientation
                                    }
                                }
                            })
                        })
                    }}
                    onSideBySide={() => this.setState({showSideBySide: !this.state.showSideBySide})}
                />

                <main id="main">
                    <Slides onSlideChange={(index) => this.setState({currentIframeIndex: index})}>
                        {this.state.iFrameData.map((iframe, i) =>
                            <IFrame
                                key={i}
                                name={iframe.name}
                                src={this.state.currentURL}
                                height={iframe.mainOrientation ? iframe.height : iframe.width}
                                width={iframe.mainOrientation ? iframe.width : iframe.height}
                                zoom={iframe.zoom}
                            />
                        )}
                    </Slides>
                    {
                        this.state.showSideBySide &&
                        <Slides onSlideChange={(index) => this.setState({currentIframeIndex: index})}>
                            {this.state.iFrameData.map((iframe, i) =>
                                <IFrame
                                    key={i}
                                    name={iframe.name}
                                    src={this.state.currentURL}
                                    height={iframe.mainOrientation ? iframe.height : iframe.width}
                                    width={iframe.mainOrientation ? iframe.width : iframe.height}
                                    zoom={iframe.zoom}
                                />
                            )}
                        </Slides>
                    }
                </main>
            </>
        );
    }
}
