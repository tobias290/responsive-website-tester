import React from "react";
import PropTypes from "prop-types";

import "../css/options-bar.css";

export default class OptionsBar extends React.Component{
    constructor(props) {
        super(props);

        this.state = {
            url: "",
        }
    }

    /**
     * Render options bar.
     */
    render() {
        return (
            <div className="options-bar">
                <form className="options-bar__form" onSubmit={(e) => {e.preventDefault(); this.props.onURLChange(this.state.url)}}>
                    <input
                        className="options-bar__form__input"
                        name="url"
                        type="text"
                        placeholder="Enter URL..."
                        value={this.state.url}
                        onChange={(e) => this.setState({url: e.target.value})}
                    />
                    <input className="options-bar__form__submit" type="submit" value="Load URL" />
                </form>

                <div className="options-bar__title">
                    <h1>{this.props.screenType} <strong>{this.props.screenWidth} x {this.props.screenHeight}</strong></h1>
                </div>

                <div className="options-bar__options">
                    <button className="option-button" onClick={this.props.onSideBySide} title="Side by Side">
                        <i className="fas fa-columns" />
                    </button>
                    <button className="option-button" onClick={this.props.onRotate} title="Rotate">
                        <i className="fas fa-sync-alt" />
                    </button>
                    <div>
                        <button className="option-button" onClick={() => this.props.onZoomChange(this.props.zoom - 0.1)} title="Zoom Out">
                            <i className="fas fa-minus" />
                        </button>
                        <span>{Math.round(this.props.zoom * 100)}%</span>
                        <button className="option-button" onClick={() => this.props.onZoomChange(this.props.zoom + 0.1)} title="Zoom In">
                            <i className="fas fa-plus" />
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

OptionsBar.propTypes = {
    screenType: PropTypes.string,
    screenWidth: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    screenHeight: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    zoom: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),

    onURLChange: PropTypes.func,
    onSideBySide: PropTypes.func,
    onRotate: PropTypes.func,
    onZoomChange: PropTypes.func,
};
