import React from "react";
import PropTypes from "prop-types";

import "../css/iframe.css";

const IFrame = (props) =>
    <iframe
        className="iframe"
        title={props.name}
        src={props.src}
        height={props.height}
        width={props.width}
        style={{transform: `scale(${props.zoom})`}}
        frameBorder="0"
    />;

export default IFrame;

IFrame.propTypes = {
    name: PropTypes.string,
    src: PropTypes.string,
    height: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    width: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    zoom: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
};

IFrame.defaultProps = {
    zoom: 1,
};
